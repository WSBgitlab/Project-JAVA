package model;

public class Cliente {
    int id_customer; //Identificação do cliente
    String cpf_cnpj; //cnpj e cpf
    String nm_customer; //Nome
    int is_active; //satus do cliente
    double vl_total; //Mostra saldo cliente
    
    //Construtor zerando valores
    public Cliente(){
     id_customer = 0; 
     cpf_cnpj = ""; 
     nm_customer = ""; 
     is_active = 0; 
     vl_total = 0;
    }

    public int getId_customer() {
        return id_customer;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public String getCpf_cnpj() {
        return cpf_cnpj;
    }

    public void setCpf_cnpj(String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }

    public String getNm_costumer() {
        return nm_customer;
    }

    public void setNm_costumer(String nm_costumer) {
        this.nm_customer = nm_costumer;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public double getVl_total() {
        return vl_total;
    }

    public void setVl_total(double vl_total) {
        this.vl_total = vl_total;
    }      
}
