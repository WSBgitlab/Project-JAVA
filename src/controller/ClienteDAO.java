
package controller;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Cliente;


public class ClienteDAO {
    public static void gravaBanco(Cliente cli){
        Connection  conn = null;
        PreparedStatement pst = null;
        //Importando Connection e PreparedStatement
        
        try{
            String url = "jdbc://localhost/sqlBack";
            String user = "root";
            String senha = "";
            
            Class.forName("org.postgresql.Driver");
            
            conn = DriverManager.getConnection(url,user,senha);
            
            pst = conn.prepareStatement("INSERT INTO Clientes (id_customer,cpf_cnpj,nm_customer,is_active,vl_total)"
                    + "VALUES (?,?,?,?,?)");
            
            pst.setString(1, cli.getNm_costumer());
            pst.setString(2, cli.getCpf_cnpj());
            pst.setInt(3, cli.getId_customer());
            pst.setInt(4, cli.getId_customer());
            pst.setDouble(5, cli.getVl_total());
            
            pst.execute();
            JOptionPane.showMessageDialog(null,"Cliente Cadastrado com Sucesso");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }finally{
            try{
                    if(conn != null){    
                        pst.close();
                        conn.close();
                    }
            }catch(SQLException e){
                  JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
    

    public ArrayList<Cliente> ler(){
        
        
        Connection  conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
    
        
        try{
                String url = "jdbc://localhost:3306/sqlBack";
                String user = "root";
                String senha = "";
                
                Class.forName("org.postgresql.Driver");
                conn = DriverManager.getConnection(url,user,senha);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }finally{
        
        }
        ArrayList<Cliente> cliente = new ArrayList();
        try{
            pst = conn.prepareStatement("SELECT * FROM Cliente");
            rs = pst.executeQuery();
            
            while(rs.next()){
                Cliente cli = new Cliente();
                
                cli.setId_customer(rs.getInt("id_costumer"));
                cli.setCpf_cnpj(rs.getString("cpf_cnpj"));
                cli.setNm_costumer(rs.getString("nm_customer"));
                cli.setIs_active(rs.getInt("is_active"));
                cli.setVl_total(rs.getDouble("vl_total"));
                cliente.add(cli);
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }finally{
            try{
            if(conn != null){
                conn.close();
                pst.close();        
            }
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
        return cliente;
    }
}
